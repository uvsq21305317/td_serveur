package td_serveur.client;

import td_serveur.serveur.Serveur;

public class Client {
	
	private String nom;
	private boolean connected;
	
	public Client()
	{
		this.nom = "unknown";
		this.connected = false;
	}
	
	public Client(String s)
	{
		this.nom = s;
	}
	
	public String getName()
	{
		return this.nom;
	}
	
	public boolean Connecte(Serveur serv)
	{
		if(this.connected)
		{
			System.out.println(this.nom + " est déjà connecté à un serveur.");
			return false;
		}
		
		if(serv.Connecter(this) == true)
		{
			this.connected = true;
			return true;
		}
		return false;
	}
	
	public void Envoyer(String msg, Serveur serv)
	{
		System.out.println(this.nom + " a envoyé " + msg);
		if(this.connected)
		{
			serv.Diffuser(msg);
		}
		else
		{
			System.out.println(this.nom + " n'est pas connecté ");
		}		
	}
	
	public void Recevoir(String msg)
	{
		System.out.println(this.nom + " a reçu " + msg);
	}

}
