package td_serveur;

import td_serveur.client.Client;
import td_serveur.serveur.Serveur;

public class Scenario {
	
	public static void main(String args[]) 
	{
		Serveur s = new Serveur();
		Client c1 = new Client("A");
		Client c2 = new Client("B");
		Client c3 = new Client("C");
		
		c1.Connecte(s);
		c2.Connecte(s);
		c3.Connecte(s);
		c3.Envoyer("Bonjour", s);
	}
}
