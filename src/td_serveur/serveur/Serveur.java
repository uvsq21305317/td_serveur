package td_serveur.serveur;

import td_serveur.client.Client;

public class Serveur {
	
	Client[] tabclient;
	int client_compte;
	
	public Serveur()
	{
		this.tabclient = new Client[100];
		client_compte = -1;
	}
	
	public boolean Connecter(Client c)
	{
		if(client_compte < 100)
		{
			client_compte++;
			tabclient[client_compte] =  c;
			System.out.println(tabclient[client_compte].getName() + " logged in");
			return true;
		}
		return false;
	}
	
	public boolean Diffuser(String msg)
	{
		for(int i = 0; i <= client_compte; i++)
		{
			tabclient[i].Recevoir(msg);
		}
		return true;
	}

}
